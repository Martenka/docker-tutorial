# The project
Simple backend server nodejs + express, API served as follows: `curl localhost:PORT/wonsz` will simply return an answer. Used language is `type script` so the project has to be compiled to `java script`.

Requirements:

- nodejs (13 or above)
- yarn (should work with npm)

## Run with nodejs

- yarn build
- yarn start or
- yarn dev (for development purpose, will watch for file changes)
- check the server: `curl localhost:3000/wonsz`

## Run with docker
This example runs the server with simple docker commands.

### Basic Dockerfile structure

```
# the base - image
FROM node:alpine

# set the workdir on the image/container
WORKDIR /usr/src/app

# copy all not ignored files from current dir to workdir
COPY . .

# install dependencies
RUN yarn install

# compile typescript into javascript
RUN yarn build

# the main run command
CMD ["yarn", "start"]
```

* Choose base image wisely, alpine is a linux with very small feature set. Smaller base image, smaller result.
* Copy only necessary things into the image.
* One can use .dockerignore file to specify elements to be excluded from the build.
* Each command creates a layer to the image.

Read: https://docs.docker.com/engine/reference/builder/

### Build an image
`docker image build -t wonsz:1.0 .`

`-t wonsz:1.0` - tag the built image with name and version

The dot at the end of the command sets the context for a build - docker will look for Dockerfile and will copy files from this location.

### Run container based on the image
`docker container run --publish 4000:3000 wonsz:1.0`

`--publish 4000:3000` - publish internal port 3000 as 4000.

`wonsz:1.0` - based on this image

### Test the dockerized app
`curl localhost:4000/wonsz`

Spot the port - 4000 is exposed from the container, but internally it runs on 3000.

### Manage containers
List all running containers:

`docker ps`

```
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                      NAMES
cdb0dc590532        wonsz:1.0           "docker-entrypoint.s…"   7 seconds ago       Up 7 seconds        0.0.0.0:4000->3000/tcp     naughty_volhard
```

Stop the container:

`docker stop naughty_volhard`

Run the container in detached mode:

`docker container run -d --publish 4000:3000 wonsz:1.0`

## Run with docker compose

docker-compose command is a convenient way to manage more than one container but also can be used to easily run a single service with well structured yaml file. Dockerfile is all about the structure of an image, docker-compose.yml will say how to run it.

### Exemplary dockerfile

```
# the base - image
FROM node:alpine

# set the workdir on the image/container
WORKDIR /usr/src/app

# copy only necessary files
COPY package.json ./
COPY yarn.lock ./

# install dependencies
RUN yarn install --frozen-lockfile

# copy other necessary files
COPY src ./src/
COPY tsconfig.json ./

# compile
RUN yarn build

# copy entrypoint
COPY docker-entrypoint.sh ./

# add executable attribute
RUN ["chmod", "+x", "/usr/src/app/docker-entrypoint.sh"]
```

This Dockerfiles does not produce runnable image. It prepares project to be run with docker-compose.

### Exemplary docker-compose

```
version: "3.7"
services:
  node-wonsz:
    container_name: node-wonsz
    build:
      dockerfile: Dockerfile.compose
      context: .
    entrypoint: ./docker-entrypoint.sh
    ports:
      - 5000:4000
    environment:
      - NODE_ENV=production
      - PORT=4000
```
* version - of the docker-compose syntax
* services - named services to be run with this compose
* dockerfile - one can specify custom-named Dockerfile
* entrypoint - the command to be run for the service
* ports - internal 4000 port will be exposed as 5000
* environment - variables

### Run

`docker-compose up --build`

(Re)build image and run specified container.

### Test

`curl http://localhost:5000/wonsz`

### Manage

`docker ps`

```
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                      NAMES
c1201f1d95f0        docker_node-wonsz   "./docker-entrypoint…"   48 seconds ago      Up 46 seconds       0.0.0.0:5000->4000/tcp     node-wonsz
```
Spot the name equal to the one specified in yaml file.

The container can be stopped with CTRL+C.

Run in detached mode: `docker-compose up --build -d`

Then stop it with: `docker-compose down`

## Run development env
This example sets containerized development environment.

### Exemplary Dockerfile

```
# the base - image
FROM node:alpine

WORKDIR /usr/src/app

COPY package.json ./
COPY yarn.lock ./
RUN yarn install
COPY tsconfig.json ./
COPY docker-entrypoint.dev.sh ./
```

This Dockerfiles does not produce runnable image. It prepares project to be run with docker-compose.

### Exemplary docker-compose

```
version: "3.7"
services:
  node-wonsz-dev:
    container_name: node-wonsz-dev
    build:
      dockerfile: Dockerfile.compose.dev
      context: .
    entrypoint: ./docker-entrypoint.dev.sh
    volumes:
      - ./src:/usr/src/app/src
    ports:
      - 5000:4000
    environment:
      - NODE_ENV=development
      - PORT=4000
```

Differences: name, dockerfile, entrypoint script and volumes.
The volume mounts `src` dir inside container at `/usr/src/app/src` location, entrypoint runs `yarn dev` which runs `nodemon` process that watches changes in files. So, all dependencies are installed in container, nodemon is also run inside container, but sources are on ones local machine. In consequence, no need to manage local environment, because it's encapsulated with container. That's sometimes convenient.

### Run

`docker-compose -f docker-compose.dev.yml up --build`

Stop and remove volume:

`docker-compose down --volumes`