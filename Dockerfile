# the base - image
FROM node:alpine

# set the workdir on the image/container
WORKDIR /usr/src/app

# copy all not ignored files from current dir to workdir
COPY . .

# install dependencies
RUN yarn install

# compile typescript into javascript
RUN yarn build

# the main run command
CMD ["yarn", "start"]