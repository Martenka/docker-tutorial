import express from 'express'
import path from 'path'

const port = process.env.PORT || 3000
const app = express()

const publicDirectoryPath = path.join(__dirname, '../public')

app.use(express.static(publicDirectoryPath))

app.get('/wonsz', (req, res) => {
  res.send({ message: 'wonsz' })
})

app.listen(port, () => {
  console.log('Gestartet ' + port)
})
